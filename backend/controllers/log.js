const Logs = require("../models/logs");
const _ = require("underscore");

const newLog = (req, res) => {
    const body = req.body;

    const log = new Logs({
        datetime: body.datetime,
        user_id: body.user_id,
        action: body.action
    });

    log.save((err, roleDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        return res.json({
            ok: true,
            role: {
                id: roleDB._id
            }
        });
    });
};

const logs = (req, res) => {
    Logs.find({})
        .sort({ datetime: -1 })
        .populate("user_id", "username")
        .exec((err, logs) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            return res.json({
                ok: true,
                items: logs.length,
                logs
            });
        });
};

const getLogs = (req, res) => {
    const { q } = req.query;

    Logs.find({ action: q })
        .sort({ datetime: -1 })
        .exec((err, logs) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            return res.json({
                ok: true,
                items: logs.length,
                logs
            });
        });
};

module.exports = {
    newLog,
    logs,
    getLogs
};
