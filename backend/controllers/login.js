const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const Users = require("../models/users");

const signin = (req, res) => {
    const body = req.body;

    Users.findOne({ username: body.username }, async (err, userDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!userDB) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: "User not found"
                }
            });
        }

        if (body.password !== userDB.password) {
            return res.status(500).json({
                ok: false,
                err: {
                    message: "Users or (password) incorrect"
                }
            });
        }

        const access_token = jwt.sign(
            {
                user: userDB
            },
            process.env.SEED,
            { expiresIn: process.env.CADUCIDAD_TOKEN }
        );

        userDB.access_token = access_token;
        await userDB.save();

        return res.json({
            ok: true,
            rol: userDB.rol_id,
            access_token
        });
    });
};

const logout = async (req, res) => {
    try {
        const { user } = req;
        if (user) {
            await Users.findByIdAndUpdate(user._id, {
                access_token: null
            }).exec();

            return res.json({ ok: true });
        } else if (req.body.id) {
            await Users.findByIdAndUpdate(req.body.id, {
                access_token: null
            }).exec();

            return res.json({ ok: true });
        }

        return res.status(500).json({
            ok: false,
            err: "User not found"
        });
    } catch (err) {
        console.log(err);
        return res.status(500).json({
            ok: false,
            err: err.message
        });
    }
};

module.exports = {
    signin,
    logout
};
