const Roles = require('../models/roles');
const _ = require('underscore');

const newRole = (req, res) => {
	const body = req.body;

	const role = new Roles({
		name: body.name,
		level: body.level
	});

	role.save((err, roleDB) => {
		if (err) {
			return res.status(500).json({
				ok: false,
				err
			});
		}

		return res.json({
			ok: true,
			role: {
				id: roleDB._id
			}
		});
	});
};

const roles = (req, res) => {
	Roles.find({}).exec((err, roles) => {
		if (err) {
			return res.status(400).json({
				ok: false,
				err
			});
		}

		return res.json({
			ok: true,
			items: roles.length,
			roles
		});
	});
};

const editRole = (req, res) => {
	let id = req.body.id;
	let body = _.pick(req.body, [ 'name', 'level' ]);

	Roles.findByIdAndUpdate(id, body, { new: true }, (err, userDB) => {
		if (err) {
			return res.status(500).json({
				ok: false,
				err
			});
		} else if (!userDB) {
			return res.status(500).json({
				ok: false,
				err: 'Role not found'
			});
		}

		return res.json({
			ok: true
		});
	});
};

const deleteRole = (req, res) => {
	let id = req.body.id;

	Roles.findByIdAndRemove(id, (err, roleDeleted) => {
		if (err) {
			return res.status(500).json({
				ok: false,
				err
			});
		}

		if (!roleDeleted) {
			return res.status(500).json({
				ok: false,
				error: {
					message: 'Role not found'
				}
			});
		}

		return res.json({
			ok: true
		});
	});
};

module.exports = {
	newRole,
	editRole,
	deleteRole,
	roles
};
