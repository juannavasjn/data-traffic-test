const Users = require("../models/users");
const _ = require("underscore");

const newUser = (req, res) => {
    const body = req.body;

    const user = new Users({
        username: body.username,
        password: body.password,
        rol_id: body.rol_id,
        status: body.status
    });

    user.save((err, userDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        return res.json({
            ok: true,
            user: {
                id: userDB._id
            }
        });
    });
};

const users = (req, res) => {
    Users.find({})
        .populate("rol_id", "name")
        .exec((err, users) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            return res.json({
                ok: true,
                items: users.length,
                users
            });
        });
};

const editUser = (req, res) => {
    let id = req.body.id;
    let body = _.pick(req.body, ["username", "password", "status", "rol_id"]);

    // res.json(id);
    Users.findByIdAndUpdate(id, body, { new: true }, (err, userDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        } else if (!userDB) {
            return res.status(500).json({
                ok: false,
                err: "User not found"
            });
        }

        return res.json({
            ok: true,
            user: userDB
        });
    });
};

const deleteUser = (req, res) => {
    let id = req.body.id;

    Users.findByIdAndRemove(id, (err, userDeleted) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!userDeleted) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: "User not found"
                }
            });
        }

        return res.json({
            ok: true,
            user: userDeleted
        });
    });
};

module.exports = {
    newUser,
    users,
    editUser,
    deleteUser
};
//module.exports = app;
