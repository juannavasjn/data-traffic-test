const dotenv = require("dotenv");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path");
const express = require("express");

const app = express();

dotenv.config();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(express.static(path.resolve(__dirname, "./public")));

// ------ Routes -------
app.use(require("./routes"));

app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "./public/index.html"));
});

//-------------------- Database -----------------------------------------------------------

mongoose.connect(
    process.env.DB_URI,
    { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
    (err, res) => {
        if (err) throw err;
        console.log("Database ONLINE" + " - " + new Date().toLocaleString());
    }
);

// Seed
// require("./seed");

app.listen(process.env.PORT, () => {
    const log =
        "Open https://localhost:" +
        process.env.PORT +
        " - " +
        new Date().toLocaleString();
    console.log(log);
});
