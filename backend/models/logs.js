const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;

let logsSchema = new Schema(
	{
		datetime: {
			type: Date,
			required: [ true, 'Name required' ]
		},
		user_id: {
			type: Schema.Types.ObjectId,
			ref: 'Users',
			required: [ true, 'User_id required' ]
		},
		action: {
			type: String,
			required: [ true, 'Action required' ]
		}
	},
	{ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } }
);

logsSchema.plugin(uniqueValidator, { message: '{PATH} must be unique' });

module.exports = mongoose.model('Logs', logsSchema);
