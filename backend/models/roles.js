const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
let Schema = mongoose.Schema;

let rolesSchema = new Schema(
    {
        name: {
            type: String,
            required: [true, "Name required"],
            unique: true
        },
        level: {
            type: Number,
            required: [true, "Lastname required"]
        }
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

rolesSchema.plugin(uniqueValidator, { message: "{PATH} must be unique" });

module.exports = mongoose.model("Roles", rolesSchema);
