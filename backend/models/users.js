const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
let Schema = mongoose.Schema;

let usersSchema = new Schema(
    {
        username: {
            type: String,
            required: [true, "Username required"],
            unique: true
        },
        password: {
            type: String,
            required: [true, "Password required"]
        },
        rol_id: {
            type: Schema.Types.ObjectId,
            ref: "Roles",
            required: [true, "Rol required"]
        },
        access_token: {
            type: String
        },
        status: {
            type: Boolean,
            default: true
        }
    },
    { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

usersSchema.plugin(uniqueValidator, { message: "{PATH} must be unique" });

usersSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
};

module.exports = mongoose.model("Users", usersSchema);
