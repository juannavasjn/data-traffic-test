const express = require('express');
const app = express();

const { verifyToken } = require('../middlewares/authentication');

const login = require('../controllers/login');
const user = require('../controllers/user');
const role = require('../controllers/role');
const log = require('../controllers/log');

app.post('/api/login', login.signin);
app.delete('/api/login', verifyToken, login.logout);

app.post('/api/users', user.newUser);
app.get('/api/users', user.users);
app.put('/api/users', user.editUser);
app.delete('/api/users', user.deleteUser);

app.post('/api/roles', role.newRole);
app.get('/api/roles', role.roles);
app.put('/api/roles', role.editRole);
app.delete('/api/roles', role.deleteRole);

app.get('/api/logs', log.logs);
app.post('/api/logs', log.newLog);

app.get('/api/logs/action', log.getLogs);

module.exports = app;
