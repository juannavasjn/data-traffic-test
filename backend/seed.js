const Roles = require("./models/roles");

Roles.create({
    name: "Admin",
    level: 99
});

Roles.create({
    name: "Consulta",
    level: 10
});
