import React from "react";
import { BrowserRouter } from "react-router-dom";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
import general from "./reducers/general";
import Router from "./routes";
import Navbar from "./components/Navbar";

const reducer = combineReducers({
    general
});

const store = createStore(reducer);

const App = () => {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <Navbar />
                <Router />
            </BrowserRouter>
        </Provider>
    );
};

export default App;
