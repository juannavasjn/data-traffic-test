import React, { useState, useEffect, useCallback, Fragment } from "react";
import { Modal, Button, Container, Row, Col } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";

export default function ErrorModal() {
    const { errorModal } = useSelector(state => state.general);

    const [show, setShow] = useState(false);
    const [state, setState] = useState({ title: "", message: "" });

    const dispatch = useDispatch();

    const setErrorModal = useCallback(
        data => dispatch({ type: "SET_ERROR_MODAL", payload: data }),
        [dispatch]
    );

    const handleClose = _ => {
        setShow(false);
        setErrorModal(false);
    };

    useEffect(
        _ => {
            if (errorModal !== false) {
                setShow(true);
                setState({
                    ...state,
                    title: errorModal.title,
                    message: errorModal.message
                });
            } else {
                setShow(errorModal);
            }
        },
        //eslint-disable-next-line react-hooks/exhaustive-deps
        [errorModal]
    );
    return (
        <Fragment>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>{state.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col>{state.message}</Col>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cerrar
                    </Button>
                </Modal.Footer>
            </Modal>
        </Fragment>
    );
}
