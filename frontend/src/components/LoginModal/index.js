import React, { useState, useEffect, useCallback, Fragment } from "react";
import { Modal, Button, Container, Row, Col, Form } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import axios from "axios";

let urlBase = process.env.REACT_APP_URL;

export default function LoginModal() {
    const history = useHistory();
    const { loginModal } = useSelector(state => state.general);

    const [show, setShow] = useState(false);
    const [username, setUsername] = useState("");
    const [pass, setPass] = useState("");

    const dispatch = useDispatch();

    const setLoginModal = useCallback(
        data => dispatch({ type: "SET_LOGIN_MODAL", payload: data }),
        [dispatch]
    );

    const setErrorModal = useCallback(
        data => dispatch({ type: "SET_ERROR_MODAL", payload: data }),
        [dispatch]
    );

    const handleClose = _ => {
        setShow(false);
        setLoginModal(false);
    };

    // const handleError = _ => {
    //     setErrorModal({
    //         title: "Login",
    //         message: "Se encontraron errores"
    //     });
    // };

    useEffect(
        _ => {
            setShow(loginModal);
        },
        [loginModal]
    );

    const handleSubmit = e => {
        e.preventDefault();

        let url = urlBase + "/api/login";

        const data = {
            username,
            password: pass
        };

        axios
            .post(url, data)
            .then(res => {
                if (res.data.ok) {
                    localStorage.setItem("access_token", res.data.access_token);
                    history.push("/gestion");
                }
            })
            .catch(err => {
                console.log(err.response.data);
                if (err.response.data.ok === false) {
                    setErrorModal({
                        title: "Login",
                        message: err.response.data.err.message
                    });
                }
            });
    };
    return (
        <Fragment>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Login</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Row>
                            <Col>
                                <Form onSubmit={handleSubmit}>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>
                                            Correo / Usuario
                                        </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder=""
                                            required
                                            onChange={e =>
                                                setUsername(e.target.value)
                                            }
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Contraseña</Form.Label>
                                        <Form.Control
                                            type="password"
                                            placeholder=""
                                            required
                                            onChange={e =>
                                                setPass(e.target.value)
                                            }
                                        />
                                    </Form.Group>

                                    <Button variant="secondary" type="submit">
                                        Acceder
                                    </Button>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Body>
            </Modal>
        </Fragment>
    );
}
