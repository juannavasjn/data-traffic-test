import React, { Fragment, useState, useEffect } from "react";
import { Modal, Button, Container, Row, Col, Form } from "react-bootstrap";
import axios from "axios";

let urlBase = process.env.REACT_APP_URL;

export default function NewRolModal({ active, update }) {
    const [show, setShow] = useState(false);
    const [level, setLevel] = useState("");
    const [rol, setRol] = useState("");
    const handleClose = _ => {
        setShow(false);
    };

    useEffect(
        _ => {
            if (active) setShow(true);
        },
        [active]
    );

    const handleSubmit = e => {
        e.preventDefault();

        const data = {
            name: rol,
            level
        };

        const url = urlBase + "/api/roles";

        axios
            .post(url, data)
            .then(res => {
                if (res.data.ok) {
                    update();
                    setShow(false);
                }
            })
            .catch(err => {
                console.log(err.response.data.err.message);
            });
    };

    return (
        <Fragment>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Roles</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Form onSubmit={handleSubmit}>
                            <Row>
                                <Col>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>Rol</Form.Label>
                                        <Form.Control
                                            type="text"
                                            onChange={e =>
                                                setRol(e.target.value)
                                            }
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Nivel</Form.Label>
                                        <Form.Control
                                            type="number"
                                            onChange={e =>
                                                setLevel(e.target.value)
                                            }
                                            required
                                        />
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button variant="secondary" type="submit">
                                        Guardar
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Container>
                </Modal.Body>
            </Modal>
        </Fragment>
    );
}
