import React, { Fragment, useState, useEffect } from "react";
import { Modal, Button, Container, Row, Col, Form } from "react-bootstrap";
import axios from "axios";

let urlBase = process.env.REACT_APP_URL;

export default function NewUserModal({ active, update }) {
    const [show, setShow] = useState(false);
    const [username, setUsername] = useState("");
    const [pass, setPass] = useState("");
    const [rol, setRol] = useState("");
    const [roles, setRoles] = useState([]);
    const [status, setStatus] = useState(false);

    useEffect(_ => {
        const url = urlBase + "/api/roles";

        axios.get(url).then(res => {
            setRoles(res.data.roles);
            setRol(res.data.roles[0]._id);
        });
    }, []);

    const handleClose = _ => {
        setShow(false);
    };

    useEffect(
        _ => {
            if (active) setShow(true);
        },
        [active]
    );

    const handleSubmit = e => {
        e.preventDefault();

        const data = {
            username,
            password: pass,
            role_id: rol,
            status
        };

        const url = urlBase + "/api/users";

        axios
            .post(url, data)
            .then(res => {
                if (res.data.ok) {
                    update();
                    setShow(false);
                }
            })
            .catch(err => {
                console.log(err.response.data.err.message);
            });
    };

    const handleSelect = e => {
        setRol(e.target.value);
    };

    return (
        <Fragment>
            <Modal show={show} onHide={handleClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Usuarios</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <Form onSubmit={handleSubmit}>
                            <Row>
                                <Col>
                                    <Form.Group controlId="formBasicEmail">
                                        <Form.Label>
                                            Correo / Usuario
                                        </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder=""
                                            onChange={e =>
                                                setUsername(e.target.value)
                                            }
                                            required
                                        />
                                    </Form.Group>

                                    <Form.Group controlId="formBasicPassword">
                                        <Form.Label>Contraseña</Form.Label>
                                        <Form.Control
                                            type="password"
                                            placeholder="Password"
                                            onChange={e =>
                                                setPass(e.target.value)
                                            }
                                            required
                                        />
                                    </Form.Group>
                                    <Form.Group controlId="formBasicRol">
                                        <Form.Label>Rol</Form.Label>

                                        <Form.Control
                                            as="select"
                                            onChange={handleSelect}
                                        >
                                            {roles.map((r, i) => (
                                                <option key={i} value={r._id}>
                                                    {r.name}
                                                </option>
                                            ))}
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="formBasicCheckbox">
                                        <Form.Check
                                            style={{ display: "inline-block" }}
                                            type="checkbox"
                                            onChange={e =>
                                                setStatus(e.target.checked)
                                            }
                                        />

                                        <Form.Label className="ml-3">
                                            Activo
                                        </Form.Label>
                                    </Form.Group>
                                </Col>
                                <Col>
                                    <Button variant="secondary" type="submit">
                                        Guardar
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </Container>
                </Modal.Body>
            </Modal>
        </Fragment>
    );
}
