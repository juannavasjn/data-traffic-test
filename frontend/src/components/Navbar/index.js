import React, { useState, useCallback } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";

import "./index.css";

export default function NavBar() {
    const dispatch = useDispatch();

    const setLoginModal = useCallback(
        data => dispatch({ type: "SET_LOGIN_MODAL", payload: data }),
        [dispatch]
    );

    const [actives, setActives] = useState({
        gestion: "",
        resumen: "active"
    });

    const handleClick = e => {
        switch (e) {
            case "resumen":
                setActives({
                    ...actives,
                    gestion: "",
                    resumen: "active"
                });
                break;
            case "gestion":
                setActives({
                    ...actives,
                    gestion: "active",
                    resumen: ""
                });
                break;
            default:
                setActives({
                    ...actives,
                    gestion: "",
                    resumen: ""
                });
                break;
        }
    };

    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand as="div">
                <Link to="/resumen" onClick={_ => handleClick("resumen")}>
                    LOGO
                </Link>
            </Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link as="div">
                    <Link
                        to="/resumen"
                        onClick={_ => handleClick("resumen")}
                        className={actives.resumen}
                    >
                        Resumen
                    </Link>
                </Nav.Link>
                <Nav.Link as="div">
                    <Link
                        to="/gestion"
                        onClick={_ => handleClick("gestion")}
                        className={actives.gestion}
                    >
                        Gestión
                    </Link>
                </Nav.Link>
            </Nav>
            <Navbar.Brand as="div">
                <Link to="#" onClick={_ => setLoginModal(true)}>
                    Login
                </Link>
            </Navbar.Brand>
        </Navbar>
    );
}
