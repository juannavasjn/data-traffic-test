import React, { useEffect, useState, Fragment } from "react";
import { Row, Col, Button } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import axios from "axios";
import NewRolModal from "../../components/Modals/NewRolModal";
import { Link } from "react-router-dom";

let urlBase = process.env.REACT_APP_URL;

export default function Roles() {
    const [roles, setRoles] = useState([]);
    const [modal, setModal] = useState(false);
    const refineRoles = roles => {
        let res = roles.map(e => ({
            id: e._id,
            name: e.name,
            level: e.level,
            total: 0
        }));

        setRoles(res);
    };

    const getRoles = _ => {
        const url = urlBase + "/api/roles";

        axios.get(url).then(res => {
            refineRoles(res.data.roles);
        });
    };

    useEffect(_ => {
        getRoles();
        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const columns = [
        {
            dataField: "id",
            text: "ID"
        },
        {
            dataField: "name",
            text: "Rol"
        },
        {
            dataField: "level",
            text: "Nivel"
        },
        {
            dataField: "total",
            text: "Total"
        }
    ];

    return (
        <Fragment>
            <Row className="mt-4">
                <NewRolModal active={modal} update={getRoles} />
                <Col md="2" className="ml-2">
                    <Row>
                        <Col>
                            <Link to="/gestion/users">Usuarios</Link>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col className="roles-active">
                            <Link to="/gestion/roles">Roles</Link>
                        </Col>
                    </Row>
                </Col>
                <Col md="8">
                    <Row>
                        <Col>Roles (2)</Col>
                        <Col className="text-right">
                            <Button onClick={_ => setModal(true)}>Nuevo</Button>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col>
                            <BootstrapTable
                                keyField="id"
                                data={roles}
                                columns={columns}
                                pagination={paginationFactory()}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Fragment>
    );
}
