import React, { useEffect, useState, Fragment } from "react";
import { Row, Col, Button } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import axios from "axios";
import NewUserModal from "../../components/Modals/NewUserModal";
import "./index.css";
import { Link } from "react-router-dom";

let urlBase = process.env.REACT_APP_URL;

export default function Usuarios() {
    const [users, setUsers] = useState([]);
    const [modal, setModal] = useState(false);

    const refineUsers = users => {
        let res = users.map(e => ({
            id: e._id,
            username: e.username,
            rol: e.rol_id.name,
            status: e.status === true ? "activo" : "inactivo"
        }));

        setUsers(res);
    };

    const getUsers = _ => {
        const url = urlBase + "/api/users";

        axios.get(url).then(res => {
            refineUsers(res.data.users);
        });
    };

    useEffect(_ => {
        getUsers();
        //eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const columns = [
        {
            dataField: "id",
            text: "ID"
        },
        {
            dataField: "username",
            text: "Correo / Usuario"
        },
        {
            dataField: "rol",
            text: "Rol"
        },
        {
            dataField: "status",
            text: "Estado"
        }
    ];

    return (
        <Fragment>
            <Row className="mt-4">
                <NewUserModal active={modal} update={getUsers} />
                <Col md="2" className="ml-2">
                    <Row>
                        <Col className="users-active">
                            <Link to="/gestion/users">Usuarios</Link>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col>
                            <Link to="/gestion/roles">Roles</Link>
                        </Col>
                    </Row>
                </Col>
                <Col md="8">
                    <Row>
                        <Col>Usuarios (3)</Col>
                        <Col className="text-right">
                            <Button onClick={_ => setModal(true)}>Nuevo</Button>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col>
                            <BootstrapTable
                                keyField="id"
                                data={users}
                                columns={columns}
                                pagination={paginationFactory()}
                            />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Fragment>
    );
}
