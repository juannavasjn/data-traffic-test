import React, { Fragment } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";

export default function Gestion() {
    const products = [
        { id: 22, name: "ssss", price: 234 },
        { id: 22, name: "ssss", price: 234 }
    ];

    function renderShowsTotal(start, to, total) {
        return (
            <p style={{ color: "blue" }}>
                From {start} to {to}, totals is {total}&nbsp;&nbsp;(its a
                customize text)
            </p>
        );
    }

    return (
        <Fragment>
            <Container>
                <Row className="mt-4">
                    <Col md="2">
                        <Row>
                            <Col>Usuarios</Col>
                        </Row>
                        <Row className="mt-2">
                            <Col>Roles</Col>
                        </Row>
                    </Col>
                    <Col md="10">
                        <Row>
                            <Col>Usuarios (3)</Col>
                            <Col>
                                <Button>Nuevo</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col></Col>
                        </Row>
                        <Row></Row>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    );
}
