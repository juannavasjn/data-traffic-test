import React, { useEffect, useState } from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import axios from "axios";
import moment from "moment";
import LoginModal from "../../components/LoginModal";
import ErrorModal from "../../components/ErrorModal";

import "./index.css";

const logsRefine = logs => {
    return logs.map(log => {
        return {
            username: log.user_id.username,
            datetime: moment(log.datetime).format("DD/MM/YYYY HH:mm"),
            action: log.action
        };
    });
};

export default function Home() {
    const [allLogs, setAllLogs] = useState([]);
    const [lastActions, setLastActions] = useState([]);
    const [lastSignins, setLastSignins] = useState([]);

    useEffect(() => {
        const url = process.env.REACT_APP_URL + "/api/logs";

        axios
            .get(url)
            .then(res => {
                setAllLogs(logsRefine(res.data.logs));
            })
            .catch(console.err);
    }, []);

    useEffect(
        _ => {
            let newLastActions = [];

            for (let i = 0; i < allLogs.length; i++) {
                if (i === 10) break;
                newLastActions.push(allLogs[i]);
            }
            setLastActions(newLastActions);
            // console.log(newLastActions);

            let newLastSignins = [];

            for (let i = 0; i < allLogs.length; i++) {
                if (newLastSignins.length === 10) break;
                if (allLogs[i].action === "iniciar sesión")
                    newLastSignins.push(allLogs[i]);
            }
            setLastSignins(newLastSignins);
            // console.log(newLastSignins);
        },
        [allLogs]
    );

    return (
        <Container className="pt-4">
            <LoginModal />
            <ErrorModal />
            <Row>
                <Col sm="12" md="6">
                    <Row>
                        <Col className="mb-3">Últimos accesos</Col>
                    </Row>
                    <Row>
                        <Col>
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Fecha / Hora</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {lastSignins.map((e, i) => (
                                        <tr key={i}>
                                            <td>{e.username}</td>
                                            <td>{e.datetime}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Col>
                <Col sm="12" md="6">
                    <Row>
                        <Col className="mb-3">Últimas acciones</Col>
                    </Row>
                    <Row>
                        <Col>
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>Usuario</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {lastActions.map((e, i) => (
                                        <tr key={i}>
                                            <td>{e.username}</td>
                                            <td>{e.action}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );
}
