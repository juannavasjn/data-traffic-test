const initialState = {
    loginModal: false,
    errorModal: false
};
export default (state = initialState, { type, ...action }) => {
    switch (type) {
        case "SET_LOGIN_MODAL":
            return { ...state, loginModal: action.payload };
        case "SET_ERROR_MODAL":
            return { ...state, errorModal: action.payload };
        default:
            return state;
    }
};
