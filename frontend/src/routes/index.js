import React from "react";
import Resumen from "../pages/Resumen";
// import Gestion from "../pages/Gestion";
import Usuarios from "../pages/Gestion/Usuarios";
import Roles from "../pages/Gestion/Roles";

import { Switch, Route, Redirect } from "react-router-dom";

const Router = _ => {
    return (
        <React.Fragment>
            <Switch>
                <Route exact path="/resumen" component={Resumen} />
                {/* <Route exact path="/gestion" component={Gestion} /> */}
                <Route exact path="/gestion/users" component={Usuarios} />
                <Route exact path="/gestion/roles" component={Roles} />

                <Redirect from="/gestion" to="/gestion/users" />

                <Redirect exact from="/" to="/resumen" />
            </Switch>
        </React.Fragment>
    );
};
export default Router;
